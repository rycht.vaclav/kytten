package com.rychtecky.app_data.feature.stringrevert.model

import com.rychtecky.app_data.Constants
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class StringRevertObject(
    @PrimaryKey
    var id: String = Constants.HAS_STRING_REVERT,
    var hasStringRevert: Boolean = false
) : RealmObject()