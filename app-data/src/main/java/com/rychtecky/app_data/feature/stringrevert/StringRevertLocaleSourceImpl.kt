package com.rychtecky.app_data.feature.stringrevert

import com.rychtecky.app_data.Constants
import com.rychtecky.app_data.feature.stringrevert.model.StringRevertObject
import com.rychtecky.app_domain.ErrorResult
import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.feature.stringrevert.StringRevertLocaleSource
import io.realm.Realm

class StringRevertLocaleSourceImpl : StringRevertLocaleSource {

    override fun loadRevertFlagFromDB(): Result<Boolean> {
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            val hasStringRevert = realm.where(StringRevertObject::class.java)
                .equalTo("id", Constants.HAS_STRING_REVERT).findFirst()?.hasStringRevert
            return if (hasStringRevert != null) Result.Success(hasStringRevert) else Result.Error(
                ErrorResult("Cannot load string revert")
            )
        }
    }

    override fun saveRevertFlagToDB(params: Boolean) {
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            realm.executeTransaction {
                it.copyToRealmOrUpdate(
                    StringRevertObject(
                        id = Constants.HAS_STRING_REVERT,
                        hasStringRevert = params
                    )
                )
            }
        }
    }
}