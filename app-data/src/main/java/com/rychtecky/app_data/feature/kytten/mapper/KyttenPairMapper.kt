package com.rychtecky.app_data.feature.kytten.mapper

import com.rychtecky.app_data.feature.kytten.model.KyttenPairDao
import com.rychtecky.app_domain.model.KyttenPair

object KyttenPairMapper {

    fun mapToKyttenPairA(kyttenPairList: List<KyttenPair>): List<KyttenPair> {
        return kyttenPairList.map { it.copy(id = it.id + "A") }
    }

    fun mapToKyttenPairB(kyttenPairList: List<KyttenPair>): List<KyttenPair> {
        return kyttenPairList.map { it.copy(id = it.id + "B") }
    }
}