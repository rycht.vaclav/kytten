package com.rychtecky.app_data.feature.kytten

import com.rychtecky.app_data.feature.kytten.mapper.KyttenPairMapper
import com.rychtecky.app_domain.ErrorResult
import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.feature.kytten.KyttenLocaleSource
import com.rychtecky.app_domain.model.KyttenPair

class KyttenLocaleSourceImpl : KyttenLocaleSource {

    var pairList = listOf<KyttenPair>()

    override fun setKyttenPairToMemory(kyttenPairList: List<KyttenPair>) {
        pairList = KyttenPairMapper.mapToKyttenPairA(kyttenPairList)
            .plus(KyttenPairMapper.mapToKyttenPairB(kyttenPairList))
    }

    override fun getKyttenPairFromMemory(): Result<List<KyttenPair>> {
        return if (pairList.isNotEmpty()) {
            Result.Success(pairList)
        } else {
            Result.Error(ErrorResult("Pair list is empty returning null"), null)
        }
    }
}