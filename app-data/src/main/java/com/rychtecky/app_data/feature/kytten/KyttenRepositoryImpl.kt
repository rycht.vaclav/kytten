package com.rychtecky.app_data.feature.kytten

import com.rychtecky.app_domain.feature.kytten.KyttenRepository
import com.rychtecky.app_domain.feature.kytten.KyttenSource
import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.feature.kytten.KyttenLocaleSource
import com.rychtecky.app_domain.model.KyttenPair


class KyttenRepositoryImpl(
    private val kyttenSource: KyttenSource,
    private val kyttenLocaleSource: KyttenLocaleSource
) : KyttenRepository {

    override suspend fun fetchKytten(): Result<List<Kytten>> {
        return kyttenSource.fetchKytten()
    }

    override fun saveKyttenPair(kyttenPairList: List<KyttenPair>) {
        kyttenLocaleSource.setKyttenPairToMemory(kyttenPairList)
    }

    override fun getKyttenPair(): Result<List<KyttenPair>> {
        return kyttenLocaleSource.getKyttenPairFromMemory()
    }
}