package com.rychtecky.app_data.feature.kytten


import com.rychtecky.app_data.network.KyttenApi
import com.rychtecky.app_domain.feature.kytten.KyttenSource
import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.exceptions.EmptyBodyException
import com.rychtecky.app_domain.model.ApiErrorResult
import com.rychtecky.app_domain.safeCall
import retrofit2.awaitResponse
import timber.log.Timber


class KyttenSourceImpl(val kyttenApi: KyttenApi) : KyttenSource {

    override suspend fun fetchKytten(): Result<List<Kytten>> {
        return safeCall(call = { fetch() }, errorMessage = "Cannot fetch Kytten - Unexpected error")
    }

    private suspend fun fetch(): Result<List<Kytten>> {

        val response = kyttenApi.getKytten().awaitResponse()

        return if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                Result.Success(data = body)
            } else {
                Result.Error(
                    ApiErrorResult(
                        code = response.code(),
                        errorMessage = response.message(),
                        apiThrowable = EmptyBodyException()
                    )
                )
            }
        } else {
            Result.Error(ApiErrorResult(code = response.code(), errorMessage = response.message()))
        }
    }
}