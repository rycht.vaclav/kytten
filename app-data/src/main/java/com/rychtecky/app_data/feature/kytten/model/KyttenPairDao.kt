package com.rychtecky.app_data.feature.kytten.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class KyttenPairDao(
    @PrimaryKey
    var id: String = "",
    var url: String = ""
): RealmObject()