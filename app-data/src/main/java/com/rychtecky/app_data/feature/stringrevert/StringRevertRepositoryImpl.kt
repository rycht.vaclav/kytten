package com.rychtecky.app_data.feature.stringrevert

import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.feature.stringrevert.StringRevertRepository
import com.rychtecky.app_domain.feature.stringrevert.StringRevertLocaleSource

class StringRevertRepositoryImpl(private val stringRevertSource: StringRevertLocaleSource) :
    StringRevertRepository {
    override fun getRevertFlag(): Result<Boolean> {
        return stringRevertSource.loadRevertFlagFromDB()
    }

    override fun updateRevertFlag(params: Boolean) {
        stringRevertSource.saveRevertFlagToDB(params)
    }
}