package com.rychtecky.app_data.di

import com.rychtecky.app_data.feature.kytten.KyttenRepositoryImpl
import com.rychtecky.app_data.feature.kytten.KyttenSourceImpl
import com.rychtecky.app_data.feature.kytten.KyttenLocaleSourceImpl
import com.rychtecky.app_data.feature.stringrevert.StringRevertLocaleSourceImpl
import com.rychtecky.app_data.feature.stringrevert.StringRevertRepositoryImpl
import com.rychtecky.app_data.realm.RealmModule
import com.rychtecky.app_domain.feature.kytten.*
import com.rychtecky.app_domain.feature.stringrevert.GetRevertFlagUseCase
import com.rychtecky.app_domain.feature.stringrevert.UpdateRevertFlagUseCase
import com.rychtecky.app_domain.feature.kytten.FetchKyttenUseCase
import com.rychtecky.app_domain.feature.kytten.KyttenLocaleSource
import com.rychtecky.app_domain.feature.kytten.KyttenRepository
import com.rychtecky.app_domain.feature.kytten.KyttenSource
import com.rychtecky.app_domain.feature.stringrevert.StringRevertLocaleSource
import com.rychtecky.app_domain.feature.stringrevert.StringRevertRepository
import org.koin.dsl.module

val appModule = module {

    single { FetchKyttenUseCase(get()) }
    single { GetRevertFlagUseCase(get()) }
    single { UpdateRevertFlagUseCase(get()) }
    single { SaveKyttenPairUseCase(get()) }
    single { LoadKyttenPairUseCase(get()) }

    single<KyttenLocaleSource> { KyttenLocaleSourceImpl() }
    single<KyttenSource> { KyttenSourceImpl(get()) }
    single<KyttenRepository> { KyttenRepositoryImpl(get(), get()) }

    single<StringRevertLocaleSource> { StringRevertLocaleSourceImpl() }
    single<StringRevertRepository> { StringRevertRepositoryImpl(get()) }

    single { RealmModule() }
}
