package com.rychtecky.app_data.di

import com.rychtecky.app_data.network.KyttenApi
import com.rychtecky.app_data.network.LoggingInterceptor
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { LoggingInterceptor() }
    factory { provideOkHttpClient(get()) }
    factory { provideKyttenApi(get()) }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl("https://api.thecatapi.com/").client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(loggingInterceptor: LoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()
}

fun provideKyttenApi(retrofit: Retrofit): KyttenApi = retrofit.create(KyttenApi::class.java)

