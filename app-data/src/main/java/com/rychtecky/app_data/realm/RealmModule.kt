package com.rychtecky.app_data.realm

import com.rychtecky.app_data.feature.kytten.model.KyttenPairDao
import com.rychtecky.app_data.feature.stringrevert.model.StringRevertObject

@io.realm.annotations.RealmModule(classes = [StringRevertObject::class, KyttenPairDao::class])
open class RealmModule {}