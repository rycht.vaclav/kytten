package com.rychtecky.app_data.network

import com.rychtecky.app_domain.model.Kytten
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface KyttenApi {

    @GET("v1/images/search")
    fun getKytten(
        @Query("api_key") auth: String = "live_twA5CDcsTcMvBmCR7wTckxVmCDLq1iS0Xk5q2DSULS13Jt0nrM8ZS05o8oYXMpT8",
        @Query("limit") limit: String = "30",
        @Query("has_breeds") hasBreeds: Boolean = true
    ): Call<List<Kytten>>
}