package com.rychtecky.kytten.viewmodels

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.feature.kytten.LoadKyttenPairUseCase
import com.rychtecky.app_domain.model.KyttenPair
import com.rychtecky.kytten.Extensions.compareCardPair
import com.rychtecky.kytten.Extensions.isGameOver
import com.rychtecky.kytten.Extensions.updateShowedValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

//TODO Need refactoring
class PairsViewModel(private val loadKyttenPairUseCase: LoadKyttenPairUseCase) : ViewModel() {

    private var cardPair: Pair<String, String> = Pair("", "")

    private val _kyttenPairsList = MutableStateFlow<List<KyttenPair>?>(null)
    val kyttenPairList = _kyttenPairsList

    private val _clicks = MutableStateFlow(0)
    val clicks = _clicks

    private val _isGameOver = MutableStateFlow(false)
    val isGameOver = _isGameOver

    private val _pairRemains = MutableStateFlow(0)
    val pairRemains = _pairRemains

    init {
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = loadKyttenPairUseCase()) {
                is Result.Success -> {
                    result.getOrNull()?.let {
                        _kyttenPairsList.emit(it.shuffled())
                        _pairRemains.emit(it.size.div(2))
                    }
                }
                is Result.Error -> {
                    Timber.d("ERROR: ${result.error}")
                }
            }
        }
    }

    //Add that user clicked on card and turn it
    private fun addClick() {
        Timber.d("Add click")
        viewModelScope.launch(Dispatchers.IO) {
            _clicks.emit(clicks.value + 1)
        }
    }

    //Reset clicks to 0 so user can continue in game
    private fun resetClicks() {
        viewModelScope.launch(Dispatchers.IO) {
            _clicks.emit(0)
        }
    }

    //After user clicked on card start to check game rules
    fun onCardClicked(id: String) {
        addClick()
        setCardVisibility(id)
    }

    //Set showed value of object to true
    private fun setCardVisibility(id: String) {
        Timber.d("setCardVisible($id)")
        _kyttenPairsList.value = _kyttenPairsList.updateShowedValue(id)
        addCardToPair(id)
    }

    //Add card to pair
    private fun addCardToPair(id: String) {
        Timber.d("addCardToPair()")
        if (cardPair.first.isEmpty()) {
            cardPair = cardPair.copy(first = id)
        } else {
            cardPair = cardPair.copy(second = id)
            checkIfCardsSame()
        }
    }

    //Check if cards are same
    private fun checkIfCardsSame() {
        Timber.d("checkIfCardsSame()")
        if (cardPair.compareCardPair()) {
            //Should inform that user found pair and make something with cards
            Timber.d("Cards are same!")
            if (kyttenPairList.isGameOver()) {
                _isGameOver.value = true
            } else {
                resetClicks()
            }

            cardPair = Pair("", "")
            setRemainsPair()
        } else {
            //Pair is not same, should turn card
            Timber.d("Different cards!")
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    setCardInPairVisibility()
                    resetClicks()
                    cardPair = Pair("", "")
                }, 2000L
            )
        }
    }

    private fun setRemainsPair() {
        _pairRemains.value = _pairRemains.value - 1
    }

    private fun setCardInPairVisibility() {
        cardPair.toList().forEach {
            _kyttenPairsList.value = _kyttenPairsList.updateShowedValue(it)
        }
    }

    fun resetGame() {
        _kyttenPairsList.value = _kyttenPairsList.value?.map { it.copy(showed = false) }?.shuffled()
        _isGameOver.value = false
        _pairRemains.value = _kyttenPairsList.value?.size?.div(2) ?: 0 //For now.. refactoring needed
        resetClicks()
    }
}
