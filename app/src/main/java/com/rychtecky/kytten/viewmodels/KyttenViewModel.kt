package com.rychtecky.kytten.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.feature.kytten.FetchKyttenUseCase
import com.rychtecky.app_domain.feature.kytten.SaveKyttenPairUseCase
import com.rychtecky.app_domain.feature.stringrevert.GetRevertFlagUseCase
import com.rychtecky.app_domain.feature.stringrevert.UpdateRevertFlagUseCase
import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.app_domain.model.KyttenPair
import com.rychtecky.kytten.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

class KyttenViewModel(
    private val fetchKyttenUseCase: FetchKyttenUseCase,
    private val updateRevertFlagUseCase: UpdateRevertFlagUseCase,
    private val getRevertFlagUseCase: GetRevertFlagUseCase,
    private val saveKyttenPairUseCase: SaveKyttenPairUseCase
) : ViewModel() {

    private val _kyttenList = MutableStateFlow<List<Kytten>?>(null)
    val kyttenList = _kyttenList

    private val _hasRevertString = MutableStateFlow(false)
    val hasRevertString = _hasRevertString

    init {
        getRevertFlag()
    }

    fun fetchKytten() {
        viewModelScope.launch(Dispatchers.IO) {
            var kyttenResult: Result<List<Kytten>>? = null

            while (kyttenResult?.isSuccess() != true) {
                kyttenResult = fetchKyttenUseCase()
                if (kyttenResult.isError()) {
                    val error = kyttenResult.errorOrNull()
                    Timber.d(error?.throwable, "Kytten isError. Data %s", error!!.message)
                }
                //delay(Constants.RETRY_TIMEOUT)
            }
            _kyttenList.emit(kyttenResult.getOrNull())
            getKyttenForPairs(kyttenResult.getOrNull())
        }
    }

    //Get 10 pairs of kytten for pairs game
    private suspend fun getKyttenForPairs(kyttenList: List<Kytten>?) {
        kyttenList?.let {
            val subList = it.map { kytten -> KyttenPair(id = kytten.id, url = kytten.url) }
                .subList(fromIndex = 0, toIndex = 6)
            saveKyttenPairUseCase(subList)
        }
    }

    fun updateRevertFlagAndPersist(newRevertFlag: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            updateRevertFlagUseCase(newRevertFlag)
            getRevertFlag()
        }
    }

    private fun getRevertFlag() {
        viewModelScope.launch(Dispatchers.IO) {
            when (val stringRevert = getRevertFlagUseCase()) {
                is Result.Error -> _hasRevertString.emit(false)
                is Result.Success -> _hasRevertString.emit(stringRevert.isSuccess())
            }
        }
    }
}

