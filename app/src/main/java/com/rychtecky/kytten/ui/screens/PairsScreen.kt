package com.rychtecky.kytten.ui.screens


import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.rychtecky.kytten.Constants
import com.rychtecky.kytten.ui.items.KyttenTopBar
import com.rychtecky.kytten.viewmodels.PairsViewModel

@Composable
fun PairsScreen(navController: NavController, viewModel: PairsViewModel) {
    Column(
        verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally
    ) {
        KyttenTopBar(
            string = Constants.TOP_BAR_PAIRS, hasBackIcon = true, navController = navController
        )

        val kytten by viewModel.kyttenPairList.collectAsState()
        val clicks by viewModel.clicks.collectAsState()
        val pairRemains by viewModel.pairRemains.collectAsState()
        val isGameOver by viewModel.isGameOver.collectAsState()

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp, horizontal = 8.dp),
            text = "Zbývající počet párů $pairRemains",
            textAlign = TextAlign.Center,
            fontSize = 16.sp
        )
        LazyVerticalGrid(
            columns = GridCells.Fixed(Constants.PAIR_GRID_COLUMN_COUNT), content = {
                kytten?.let {
                    items(items = it) { kytten ->
                        Box(
                            modifier = Modifier
                                .padding(8.dp)
                                .aspectRatio(1f)
                                .clip(RoundedCornerShape(5.dp))
                                .background(Transparent)
                                .clickable {
                                    if (clicks < 2 && !kytten.showed) {
                                        viewModel.onCardClicked(kytten.id)
                                    }
                                }
                        ) {
                            if (kytten.showed) {
                                AsyncImage(
                                    model = kytten.url,
                                    placeholder = painterResource(id = com.rychtecky.kytten.R.drawable.ic_launcher_foreground),
                                    contentDescription = "",
                                    contentScale = ContentScale.Crop,
                                    modifier = Modifier.fillMaxSize()
                                )
                            } else {
                                Box(
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .background(Black)
                                )
                            }
                        }
                    }
                }
            }
        )
        if (isGameOver) {
            Button(onClick = { viewModel.resetGame() }) {
                Text(modifier = Modifier.padding(all = 8.dp), text = "RESTART")
            }
        }
    }
}



