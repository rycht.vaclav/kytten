package com.rychtecky.kytten.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.rychtecky.kytten.Constants

/**
 * Created by Václav Rychtecký on 03/24/2023
 */

@Composable
fun SwitchRow(modifier: Modifier, func: (Boolean) -> Unit, checkState: Boolean = false) {
    var checked by remember { mutableStateOf(checkState) }
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(modifier = Modifier.padding(all = Constants.DP_8), text = "Revert string")
        Switch(
            checked = checked,
            onCheckedChange = {
                checked = it
                func(it)
            }
        )
    }
}