package com.rychtecky.kytten.ui.items

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.kytten.Constants
import com.rychtecky.kytten.Extensions.revert
import com.rychtecky.kytten.viewmodels.KyttenViewModel
import com.rychtecky.kytten.R
import timber.log.Timber

@Composable
fun KyttenListItem(kytten: Kytten, viewModel: KyttenViewModel) {
    Column(
        modifier = Modifier
            .padding(horizontal = Constants.DP_4)
            .size(Constants.DP_120)
            .clip(shape = RoundedCornerShape(Constants.DP_4))
    ) {

        // Flag for reverting string
        val hasRevertString by viewModel.hasRevertString.collectAsState()
        Timber.d("hasRevertString: $hasRevertString")

        AsyncImage(
            model = kytten.url,
            placeholder = painterResource(id = R.drawable.ic_launcher_foreground),
            contentDescription = "",
            contentScale = ContentScale.FillWidth,
            modifier = Modifier
                .height(Constants.DP_80)
                .fillMaxWidth(),
        )
        Column(
            modifier = Modifier
                .background(color = Color.LightGray)
                .fillMaxWidth()
        ) {
            Text(
                modifier = Modifier.padding(start = Constants.DP_4),
                text = kytten.breeds[0].name.revert(hasRevertString),
                fontSize = Constants.SP_8
            )
            Text(
                modifier = Modifier.padding(start = Constants.DP_4, bottom = Constants.DP_4),
                text = kytten.breeds[0].origin.revert(hasRevertString),
                fontSize = Constants.SP_6
            )
        }
    }
}