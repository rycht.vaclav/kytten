package com.rychtecky.kytten.ui.items

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.SignalCellularConnectedNoInternet4Bar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import com.rychtecky.kytten.Constants

@Composable
fun NoInternetConnection() {
    Box(
        modifier = Modifier
            .padding(Constants.DP_32)
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Image(
                modifier = Modifier.size(Constants.DP_120),
                contentScale = ContentScale.FillBounds,
                imageVector = Icons.Default.SignalCellularConnectedNoInternet4Bar,
                contentDescription = ""
            )
            Text(
                modifier = Modifier.padding(top = Constants.DP_120),
                text = Constants.NO_CONNECTION_MESSAGE
            )
        }

    }
}