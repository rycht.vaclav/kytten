package com.rychtecky.kytten.ui.items

import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NavController
import com.rychtecky.kytten.Constants
import com.rychtecky.kytten.ui.Screen
import timber.log.Timber

/**
 * Created by Václav Rychtecký on 03/24/2023
 */
@Composable
fun KyttenTopBar(
    string: String,
    hasBackIcon: Boolean = false,
    hasActions: Boolean = false,
    navController: NavController
) {

    var dropDownMenuExpanded by remember { mutableStateOf(false) }

    TopAppBar(
        title = { Text(text = string) },
        navigationIcon = {
            if (hasBackIcon) {
                IconButton(onClick = {
                    Timber.d("On back pressed")
                    navController.navigateUp()
                }) {
                    Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "Go back")
                }
            }
        },
        actions = {
            if (hasActions) {
                TopAppBarActionButton(
                    imageVector = Icons.Outlined.MoreVert,
                    description = "Options"
                ) {
                    dropDownMenuExpanded = true
                }
                DropdownMenu(
                    expanded = dropDownMenuExpanded,
                    onDismissRequest = { dropDownMenuExpanded = false }
                ) {
                    DropdownMenuItem(onClick = {
                        Timber.d("Item click")
                        navController.navigate(Screen.SettingsScreen.route)
                    }) {
                        Text(text = Constants.TOP_BAR_SETTINGS)
                    }
                    DropdownMenuItem(onClick = {
                        Timber.d("Item click")
                        navController.navigate(Screen.PairsScreen.route)
                    }) {
                        Text(text = Constants.TOP_BAR_PAIRS)
                    }
                }
            }
        }
    )
}

@Composable
fun TopAppBarActionButton(
    imageVector: ImageVector,
    description: String,
    onClick: () -> Unit
) {
    IconButton(onClick = {
        onClick()
    }) {
        Icon(imageVector = imageVector, contentDescription = description)
    }
}
