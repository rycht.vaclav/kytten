package com.rychtecky.kytten.ui.items

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.rychtecky.kytten.Constants
import com.rychtecky.kytten.ui.components.SwitchRow
import com.rychtecky.kytten.viewmodels.KyttenViewModel

@Composable
fun SettingsScreen(navController: NavController, viewModel: KyttenViewModel) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        // Values for fill from DB
        val checkState by viewModel.hasRevertString.collectAsState()
        // Top app bar
        KyttenTopBar(
            string = Constants.TOP_BAR_SETTINGS,
            hasBackIcon = true,
            navController = navController
        )
        // Switch for reverting strings
        SwitchRow(modifier = Modifier.fillMaxWidth(),  func = { viewModel.updateRevertFlagAndPersist(it) }, checkState = checkState)
    }
}
