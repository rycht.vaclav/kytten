package com.rychtecky.kytten.ui.items

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.*
import androidx.compose.ui.unit.dp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.kytten.Constants
import com.rychtecky.kytten.viewmodels.KyttenViewModel
import kotlinx.coroutines.delay

@Composable
fun KyttenList(kyttenList: List<Kytten>, viewModel: KyttenViewModel) {

    var refreshing by remember { mutableStateOf(false) }

    LaunchedEffect(refreshing) {
        if (refreshing) {
            delay(Constants.SWIPE_REFRESH_DELAY)
            refreshing = false
        }
    }

    SwipeRefresh(
        state = rememberSwipeRefreshState(isRefreshing = refreshing),
        onRefresh = {
            refreshing = true
            viewModel.fetchKytten()
        }
    ) {
        LazyVerticalGrid(
            columns = GridCells.Fixed(Constants.GRID_COLUMN_COUNT),
            contentPadding = PaddingValues(all = Constants.DP_8)
        ) {
            items(kyttenList) { item ->
                KyttenListItem(kytten = item, viewModel = viewModel)
            }
        }
    }
}