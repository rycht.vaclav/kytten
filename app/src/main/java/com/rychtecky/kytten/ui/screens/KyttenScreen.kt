package com.rychtecky.kytten.ui.items

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.rychtecky.kytten.Constants
import com.rychtecky.kytten.viewmodels.KyttenViewModel

//TODO need refactoring
@Composable
fun KyttenScreen(
    navController: NavController,
    viewModel: KyttenViewModel
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        KyttenTopBar(
            string = Constants.TOP_BAR_SETTINGS,
            hasActions = true,
            navController = navController
        )

        val kytten by viewModel.kyttenList.collectAsState()

        kytten?.let {
            KyttenList(kyttenList = it, viewModel = viewModel)
        } ?: run {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                CircularProgressIndicator()
            }
        }
    }
}
