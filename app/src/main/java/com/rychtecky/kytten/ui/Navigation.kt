package com.rychtecky.kytten.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rychtecky.kytten.ui.items.KyttenScreen
import com.rychtecky.kytten.ui.items.SettingsScreen
import com.rychtecky.kytten.ui.screens.PairsScreen
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.compose.koinViewModel
import timber.log.Timber

/**
 * Created by Václav Rychtecký on 03/24/2023
 */
@Composable
fun Navigation() {

    val viewModelStoreOwner = checkNotNull(LocalViewModelStoreOwner.current) {
        "No ViewModelStoreOwner was provided via LocalViewModelStoreOwner"
    }

    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Screen.MainScreen.route) {
        composable(route = Screen.MainScreen.route) {
            CompositionLocalProvider(
                LocalViewModelStoreOwner provides viewModelStoreOwner
            ) {
                KyttenScreen(
                    navController = navController,
                    viewModel = koinViewModel(),
                )
            }
        }
        composable(route = Screen.SettingsScreen.route) {
            CompositionLocalProvider(
                LocalViewModelStoreOwner provides viewModelStoreOwner
            ) {
                SettingsScreen(
                    navController = navController,
                    viewModel = koinViewModel(),
                )
            }
        }
        composable(
            route = Screen.PairsScreen.route,
        ) {
            CompositionLocalProvider(
                LocalViewModelStoreOwner provides viewModelStoreOwner
            ) {
                PairsScreen(
                    navController = navController,
                    viewModel = koinViewModel()
                )
            }
        }
    }
}
