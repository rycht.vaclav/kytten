package com.rychtecky.kytten.ui

import com.rychtecky.kytten.Constants

/**
 * Created by Václav Rychtecký on 03/24/2023
 */
sealed class Screen(val route: String) {
    object MainScreen : Screen(Constants.MAIN_SCREEN)
    object SettingsScreen : Screen(Constants.SETTINGS_SCREEN)
    object PairsScreen : Screen(Constants.PAIRS_SCREEN)
}
