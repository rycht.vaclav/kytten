package com.rychtecky.kytten

import com.rychtecky.app_domain.model.KyttenPair
import com.rychtecky.kytten.Extensions.updateShowedValue
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * Created by Václav Rychtecký on 03/23/2023
 */
object Extensions {

    /**
     * Extension for string to get reverted text
     *
     * @param revertStringFlag value which decide about reverting (true = revert, false = no revert)
     * @param string example "Ó jaká to krása všech krás"
     * @return reverted example "sáɹʞ ɥɔǝšʌ ɐsáɹʞ oʇ áʞɐɾ Ó"
     *
     * Char that isn't in [Constants.revertedAlphabetMap] remains same.
     */
    fun String.revert(revertStringFlag: Boolean = true): String {
        val revertedBuilder = StringBuilder(this.length)
        return if (revertStringFlag) {
            // Find reverted character and add to new string
            this.forEach { char ->
                revertedBuilder.append(Constants.revertedAlphabetMap[char] ?: char)
            }
            // Used reversed to set characters in reversed order
            revertedBuilder.reversed().toString()
        } else {
            this
        }
    }

    /*
    * Extensions for compare two strings in cardPair
    *
    * Drop last char is there because i add A or B character to id when set into memory
    * */
    fun Pair<String, String>.compareCardPair(): Boolean {
        return this.first.dropLast(1) == this.second.dropLast(1)
    }

    /*
    * Extensions for update boolean value in Pair game, return new list for update UI
    *
    * This update showed value.
    * */
    fun MutableStateFlow<List<KyttenPair>?>.updateShowedValue(id: String): List<KyttenPair>? {
        return this.value?.let {
            it.map { kyttenPair ->
                if (kyttenPair.id == id) {
                    kyttenPair.copy(showed = !kyttenPair.showed)
                } else {
                    kyttenPair
                }
            }
        }
    }

    /*
    * Extensions which let you know if every pair was found
    * */
    fun MutableStateFlow<List<KyttenPair>?>.isGameOver(): Boolean {
        this.value?.forEach {
            if (!it.showed) {
                return false
            }
        } ?: return false
        return true
    }
}
