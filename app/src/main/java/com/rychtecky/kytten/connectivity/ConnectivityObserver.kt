package com.rychtecky.kytten.connectivity

import kotlinx.coroutines.flow.Flow

interface ConnectivityObserver {

    fun observe(): Flow<Status>

    enum class Status { UNAVAILABLE, AVAILABLE, LOST }
}
