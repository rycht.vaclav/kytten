package com.rychtecky.kytten

import android.app.Application
import com.rychtecky.app_data.di.appModule
import com.rychtecky.app_data.di.networkModule
import com.rychtecky.app_data.realm.RealmModule
import com.rychtecky.kytten.di.viewModelModule
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin
import timber.log.Timber

class KyttenApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        initKoin()

        Realm.init(this)

        val realmModule = getKoinInstance<RealmModule>()
        val realmConfig = RealmConfiguration.Builder()
            .name("kytten.realm")
            .modules(realmModule)
            .deleteRealmIfMigrationNeeded()
            .schemaVersion(1)
            .build()

        Realm.setDefaultConfiguration(realmConfig)
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@KyttenApplication)
            modules(
                appModule + networkModule + viewModelModule
            )
        }
    }

    inline fun <reified T : Any> getKoinInstance(): T {
        return object : KoinComponent {
            val value: T by inject()
        }.value
    }
}