package com.rychtecky.kytten.di

import com.rychtecky.kytten.viewmodels.KyttenViewModel
import com.rychtecky.kytten.viewmodels.PairsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelModule: Module = module {
    viewModel { KyttenViewModel(get(), get(), get(), get()) }
    viewModel { PairsViewModel(get()) }
}
