package com.rychtecky.kytten

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rychtecky.app_domain.model.Kytten

object Constants {

    const val RETRY_TIMEOUT = 5000L
    const val TIMEOUT = 25000L

    const val SWIPE_REFRESH_DELAY = 1500L
    const val GRID_COLUMN_COUNT = 2
    const val PAIR_GRID_COLUMN_COUNT = 4

    const val GENERIC_ERROR_MESSAGE = "Something goes wrong"
    const val LOST_CONNECTION_MESSAGE = "Internet connection interrupted"
    const val NO_CONNECTION_MESSAGE = "No internet connection"
    const val TRY_AGAIN_MESSAGE = "Please try again later!"

    val DP_120 = 120.dp
    val DP_80 = 80.dp
    val DP_32 = 32.dp
    val DP_8 = 8.dp
    val DP_4 = 4.dp

    val SP_8 = 8.sp
    val SP_6 = 6.sp

    val kytten = listOf<Kytten>()

    const val TOP_BAR_SETTINGS = "SETTINGS"
    const val TOP_BAR_PAIRS = "PAIRS GAME"

    const val MAIN_SCREEN = "main_screen"
    const val SETTINGS_SCREEN = "settings_screen"
    const val PAIRS_SCREEN = "pair_screen"

    val revertedAlphabetMap = mapOf(
        'a' to 'ɐ', 'b' to 'q', 'c' to 'ɔ', 'd' to 'p', 'e' to 'ǝ', 'f' to 'ɟ', 'g' to 'ƃ',
        'h' to 'ɥ', 'i' to 'ᴉ', 'j' to 'ɾ', 'k' to 'ʞ', 'l' to 'l', 'm' to 'ɯ', 'n' to 'u',
        'o' to 'o', 'p' to 'd', 'q' to 'b', 'r' to 'ɹ', 's' to 's', 't' to 'ʇ', 'u' to 'n',
        'v' to 'ʌ', 'w' to 'ʍ', 'x' to 'x', 'y' to 'ʎ', 'z' to 'z', 'A' to '∀', 'B' to 'B',
        'C' to 'Ɔ', 'D' to 'D', 'E' to 'Ǝ', 'F' to 'Ⅎ', 'G' to 'פ', 'H' to 'H', 'I' to 'I',
        'J' to 'ſ', 'K' to 'ʞ', 'L' to '˥', 'M' to 'W', 'N' to 'N', 'O' to 'O', 'P' to 'Ԁ',
        'Q' to 'Q', 'R' to 'R', 'S' to 'S', 'T' to '┴', 'U' to '∩', 'V' to 'Λ', 'W' to 'M',
        'X' to 'X', 'Y' to '⅄', 'Z' to 'Z', '0' to '0', '1' to 'Ɩ', '2' to 'ᄅ', '3' to 'Ɛ',
        '4' to 'ㄣ', '5' to 'ϛ', '6' to '9', '7' to 'ㄥ', '8' to '8', '9' to '6', ',' to "'",
        '.' to '˙', '?' to '¿', '!' to '¡', '"' to '„', "'" to ',', '`' to ',', '(' to ')',
        ')' to '(', '[' to ']', ']' to '[', '{' to '}', '}' to '{', '<' to '>', '>' to '<',
        '&' to '⅋', '_' to '‾'
    )
}
