package com.rychtecky.kytten.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import com.rychtecky.kytten.ui.Navigation
import com.rychtecky.kytten.ui.theme.KyttenTheme
import com.rychtecky.kytten.viewmodels.KyttenViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class KyttenActivity : ComponentActivity() {

    private val viewModel: KyttenViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.fetchKytten()

        setContent {
            KyttenTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Navigation()
                }
            }
        }
    }
}
