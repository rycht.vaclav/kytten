package com.rychtecky.app_domain.exceptions

import java.io.IOException

class EmptyBodyException : IOException("Response with empty body")