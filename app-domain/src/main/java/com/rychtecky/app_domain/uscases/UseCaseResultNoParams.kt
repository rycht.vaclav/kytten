package com.rychtecky.app_domain.uscases

import com.rychtecky.app_domain.Result

abstract class UseCaseResultNoParams<out T : Any> : UseCase<Result<T>, Unit>() {
    suspend operator fun invoke() = super.invoke(Unit)
}