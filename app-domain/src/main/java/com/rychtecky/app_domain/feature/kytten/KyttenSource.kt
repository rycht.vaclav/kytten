package com.rychtecky.app_domain.feature.kytten

import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.app_domain.Result

interface KyttenSource {
    suspend fun fetchKytten(): Result<List<Kytten>>
}