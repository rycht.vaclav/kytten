package com.rychtecky.app_domain.feature.kytten

import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.model.KyttenPair

interface KyttenRepository {
    suspend fun fetchKytten(): Result<List<Kytten>>
    fun saveKyttenPair(kyttenPairList: List<KyttenPair>)
    fun getKyttenPair(): Result<List<KyttenPair>>
}