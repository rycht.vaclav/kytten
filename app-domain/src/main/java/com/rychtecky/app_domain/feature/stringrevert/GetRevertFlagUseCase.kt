package com.rychtecky.app_domain.feature.stringrevert

import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.uscases.UseCaseResultNoParams

class GetRevertFlagUseCase(private val stringRevertRepository: StringRevertRepository) :
    UseCaseResultNoParams<Boolean>() {
    override suspend fun doWork(params: Unit): Result<Boolean> =
        stringRevertRepository.getRevertFlag()
}