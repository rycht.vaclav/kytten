package com.rychtecky.app_domain.feature.kytten

import com.rychtecky.app_domain.model.KyttenPair
import com.rychtecky.app_domain.uscases.UseCase

class SaveKyttenPairUseCase(private val kyttenRepository: KyttenRepository) :
    UseCase<Unit, List<KyttenPair>>() {
    override suspend fun doWork(params: List<KyttenPair>) = kyttenRepository.saveKyttenPair(params)
}