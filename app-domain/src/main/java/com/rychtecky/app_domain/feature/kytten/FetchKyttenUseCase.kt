package com.rychtecky.app_domain.feature.kytten

import com.rychtecky.app_domain.model.Kytten
import com.rychtecky.app_domain.uscases.UseCaseResultNoParams

class FetchKyttenUseCase(private val kyttenRepository: KyttenRepository) :
    UseCaseResultNoParams<List<Kytten>>() {

    override suspend fun doWork(params: Unit) = kyttenRepository.fetchKytten()
}