package com.rychtecky.app_domain.feature.kytten

import com.rychtecky.app_domain.Result
import com.rychtecky.app_domain.model.KyttenPair

interface KyttenLocaleSource {
    fun setKyttenPairToMemory(kyttenPairList: List<KyttenPair>)
    fun getKyttenPairFromMemory(): Result<List<KyttenPair>>
}