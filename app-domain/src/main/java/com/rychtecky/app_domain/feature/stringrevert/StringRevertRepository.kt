package com.rychtecky.app_domain.feature.stringrevert

interface StringRevertRepository {
    fun getRevertFlag(): com.rychtecky.app_domain.Result<Boolean>
    fun updateRevertFlag(params: Boolean)
}