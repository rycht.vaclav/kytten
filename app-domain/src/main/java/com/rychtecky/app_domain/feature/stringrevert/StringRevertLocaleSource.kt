package com.rychtecky.app_domain.feature.stringrevert

interface StringRevertLocaleSource {
    fun loadRevertFlagFromDB(): com.rychtecky.app_domain.Result<Boolean>
    fun saveRevertFlagToDB(params: Boolean)
}