package com.rychtecky.app_domain.feature.stringrevert

import com.rychtecky.app_domain.uscases.UseCase

class UpdateRevertFlagUseCase(private val stringRevertRepository: StringRevertRepository) :
    UseCase<Unit, Boolean>() {
    override suspend fun doWork(params: Boolean) = stringRevertRepository.updateRevertFlag(params)
}