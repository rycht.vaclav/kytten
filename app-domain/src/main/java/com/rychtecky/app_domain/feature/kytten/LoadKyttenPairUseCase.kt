package com.rychtecky.app_domain.feature.kytten

import com.rychtecky.app_domain.model.KyttenPair
import com.rychtecky.app_domain.uscases.UseCaseResultNoParams

class LoadKyttenPairUseCase(private val kyttenRepository: KyttenRepository) :
    UseCaseResultNoParams<List<KyttenPair>>() {
    override suspend fun doWork(params: Unit): com.rychtecky.app_domain.Result<List<KyttenPair>> =
        kyttenRepository.getKyttenPair()
}