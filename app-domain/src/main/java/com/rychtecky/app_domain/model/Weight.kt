package com.rychtecky.app_domain.model

data class Weight(
    val imperial: String,
    val metric: String
)