package com.rychtecky.app_domain.model

import com.rychtecky.app_domain.ErrorResult

data class ApiErrorResult(
    val code: Int,
    val errorMessage: String? = null,
    val apiThrowable: Throwable? = null
) : ErrorResult(errorMessage, apiThrowable)