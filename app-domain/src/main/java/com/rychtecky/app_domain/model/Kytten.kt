package com.rychtecky.app_domain.model

data class Kytten(
    val breeds: List<Breeds>,
    val id: String,
    val url: String,
    val width: String,
    val height: String
)