package com.rychtecky.app_domain.model

data class KyttenPair(
    val id: String,
    val url: String,
    var showed: Boolean = false,
    var found: Boolean = false
)